angular.module("resetPasswordApp").service("LoginService", function ($http, apiURL) {
    var self = this;
    self.resetPassword = function (data, token) {
        return $http({
            method: 'PUT',
            url: apiURL.FORGOT_PASSWORD_RESET,
            headers: {
                'JWT-TOKEN': token,
                'new-password': data.newPassword
            }
        });
    };

    self.getRestoreToken = function (url_restore) {
        return $http({
            method: 'GET',
            url: url_restore
        });
    };
});