angular.module("resetPasswordApp", ['authConfig', 'appConfig', 'washerCommon']);
angular.module("resetPasswordApp").controller("resetPasswordController", function ($scope, $window, LoginService, authService, $timeout) {
    $scope.reset = {};
    /**
     * Possible keys: 
     * showResetPassword, showResetPasswordResult
     */
    $scope.showPage = function(pageKey){
        $scope.showResetPassword = false;
        $scope.showResetPasswordResult = false;
        $scope[pageKey] = true;
    };
    $scope.showPage('showResetPassword');

    $scope.backToLogin = function(){
        $window.location.href = '/login/';
    };

    $scope.resetPassword = function ($event) {
        $event.preventDefault();
        $scope.form.$submitted = true;
        if ($scope.form.$valid) {
            var restoreUrl = getUrlRestoreParameter();
            $scope.loading = true;
            LoginService.getRestoreToken(restoreUrl).then(function(responseToken) {
                LoginService.resetPassword($scope.reset, responseToken.data).then(function (response) {
                    $scope.loading = false;
                    authService.saveAuthorization(responseToken.data);
                    $scope.error = false;
                    $scope.showPage('showResetPasswordResult');
                    $timeout(function(){
                        $window.location.href = '/';
                    }, 2000);
                }, function (error) {
                    $scope.loading = false;
                    $scope.error = error.status;
                    $scope.showPage('showResetPasswordResult');
                });
            }, function (error) {
                $scope.loading = false;
                $scope.invalidCredentials = true;
            });
        }
    };
});

function getUrlRestoreParameter() {
    return window.location.search.substring(1).replace('restore_url=', '');
}