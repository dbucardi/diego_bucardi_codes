function accessTokenHttpInterceptor($window, $q) {
    return {
        request: function ($config) {
            if($config.url.indexOf('.html') === -1){
                var token = $window.sessionStorage.accessToken;
                $config.headers['JWT-TOKEN'] = token;
            }
            return $config;
        },
        response: function (response) {
            return response;
        },
        responseError: function (rejection) {
            if(rejection.data === 'Unauthorized'){
                $window.location.href = '/login';
            }
            return $q.reject(rejection);;
        }

    };
}

accessTokenHttpInterceptor.$inject = ['$window', '$q'];

function httpInterceptorRegistry($httpProvider) {
    $httpProvider.interceptors.push('accessTokenHttpInterceptor');
}

httpInterceptorRegistry.$inject = ['$httpProvider'];

angular
    .module("washerApp")
    .config(httpInterceptorRegistry)
    .factory('accessTokenHttpInterceptor', accessTokenHttpInterceptor);