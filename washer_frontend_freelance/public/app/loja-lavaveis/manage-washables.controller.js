angular.module("washerApp").controller('GerenciaLavaveisLojaCtrl', function ($scope, gridOptions, $location, ApiService, $filter, $routeParams, authService) {
    $scope.id = $routeParams.id;
    initGridOptions();
    getStoreWashables();

    function initGridOptions() {
        $scope.gridOptions = angular.extend(angular.copy(gridOptions), {
            enableCellEditOnFocus: true,
            enableVerticalScrollbar: 2,
            columnDefs: [
                {
                    name: 'enabled',
                    displayName: '',
                    enableCellEdit: false,
                    type: 'bollean',
                    cellTemplate: '<input class="form-control" type="checkbox" ng-model="row.entity.enabled">',
                    width: 40,
                },
                {
                    name: 'name',
                    displayName: 'Nome da peça *',
                    enableCellEdit: true,
                    cellEditableCondition: isNameCellEditable
                },
                {
                    name: 'services_complete',
                    displayName: 'COMPLETO *',
                    cellFilter: 'currency : "R$" : 2',
                    type: 'number',
                    cellEditableCondition: isCellEditable
                },
                {
                    name: 'services_recovery',
                    displayName: 'RECUPERAR',
                    cellFilter: 'currency : "R$" : 2',
                    type: 'number',
                    cellEditableCondition: isCellEditable
                },
                {
                    name: 'services_ironing',
                    displayName: 'PASSAR',
                    cellFilter: 'currency : "R$" : 2',
                    type: 'number',
                    cellEditableCondition: isCellEditable
                },
                {
                    name: 'actions', displayName: '',
                    enableCellEdit: false,
                    width: 80, cellTemplate: 'app/loja-lavaveis/actions.html'
                }
            ],
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        });
    }

    function isCellEditable($rowScope) {
        return $rowScope.row.entity.enabled;
    }

    function isNameCellEditable($rowScope){
        return $rowScope.row.entity.enabled && !$rowScope.row.entity.default;
    }

    
    function initPermitions(){
        var loggedUser = authService.getUser();
        if(loggedUser.attributes === 'Administrator'){
            $scope.canEdit_services_qtd = true;
            getFranchises();
            $scope.canSelectFranchise = true;
        } else if(loggedUser.attributes === 'Franchise Manager'){
            $scope.canEdit_services_qtd = true;
            loadDefaultFrachise(loggedUser);
        }
    }

    function getStoreWashables() {
        $scope.loading = true;
        ApiService.getStoreWashables($scope.id).then(function (response) {
            $scope.error = false;
            $scope.loading = false;
            $scope.data = response.data;
            $scope.filter();
        }, function (error) {
            $scope.loading = false;
            $scope.error = true;
        });
    }

    $scope.save = function(){
        $scope.loading = true;
        ApiService.saveStoreWashables($scope.id, $scope.data).then(function (response) {
            $scope.error = false;
            $scope.loading = false;
            $scope.filter();
        }, function (error) {
            $scope.loading = false;
            $scope.error = error.status;
            $scope.errorMessage = error.message;
            angular.element('html').scrollTop(0);
            angular.element('.ui-grid-viewport').scrollTop(0);
        });
    };

    $scope.filter = function () {
        $scope.gridOptions.data = $filter('filter')($scope.data, $scope.filterValue);
        $scope.gridApi.grid.refresh();
    };

    $scope.onEditClick = function (entity) {
        $location.path('/lojas/' + entity._id);
    };

    $scope.onDeleteWashable = function(entity){
        if(entity._id){
            if(confirm('Tem certeza que deseja remover esta peça?')){
                deleteWashable(entity);
            }
        } else{
            deleteWashable(entity);
        }
    };

    function deleteWashable(entity){
        $scope.data.splice($scope.data.indexOf(entity), 1);
        $scope.filter();
    }

    $scope.onAddNewWashable = function(){
        var newItem = { 
            "name": "", 
            "icon": "", 
            "enabled": true, 
            "default": false, 
            "services_complete": 0, 
            "services_ironing": 0, 
            "services_recovery": 0 
        };
        $scope.data.unshift(newItem);
        $scope.filterValue = '';
        $scope.filter();
        angular.element('html').scrollTop(0);
        angular.element('.ui-grid-viewport').scrollTop(0);
    };
});