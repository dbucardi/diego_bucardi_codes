angular.module('washerCommon').directive('fieldWrapper', function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            form : '=',
            fieldName : '@',
            fieldTip: '@',
            fieldTitle : '@',
            isRequiredField: '@'
        },
        templateUrl: 'app/common/field-wrapper.html'
    };
});