angular.module("washerCommon").service('MultipartFormService', [ '$http', function ($http) {
    var self = this;
    var config = {
        transformRequest: angular.identity,
        headers: { 'Content-Type': undefined}
    };

    self.post = function(url, data){
        var formData = createFormData(data);
        return $http.post(url, formData, config);
    }

    self.put = function(url, data){
        var formData = createFormData(data);
        return $http.put(url, formData, config);
    }

    function createFormData(data){
        var formData = new FormData();
        for(var key in data){
            formData.append(key, data[key]);
        }
        return formData;
    }
}]);