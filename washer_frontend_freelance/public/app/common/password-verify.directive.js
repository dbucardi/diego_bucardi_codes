angular.module("washerCommon").directive("passwordVerify", function () {
    return {
        require: "ngModel",
        scope: {
            passwordVerify: '='
        },
        link: function (scope, element, attrs, ctrl) {
            scope.$watch(function () {
                return ctrl.$viewValue;
            }, function (value) {
                passwordVerify();
            });

            scope.$watch(function () {
                return scope.passwordVerify;
            }, function () {
                return passwordVerify();
            });

            function passwordVerify(value) {
                if (scope.passwordVerify !== ctrl.$viewValue) {
                    ctrl.$setValidity("passwordVerify", false);
                } else {
                    ctrl.$setValidity("passwordVerify", true);
                }
            }
        }
    };
});