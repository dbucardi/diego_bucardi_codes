angular.module("washerCommon").controller('MenuCtrl', function ($scope, $location, authService) {
    $scope.menuItems = authService.getMenuItems();
    $scope.isMenuSelected = function(section){
        return $location.url().indexOf(section) > 0;
    };
});