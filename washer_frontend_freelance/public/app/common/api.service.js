angular.module("washerCommon").service('ApiService', ['apiURL', 'MultipartFormService', '$http', '$q', function (apiURL, MultipartFormService, $http, $q) {
    var self = this;

    /**
     * FRANQUIAS *****
     */
    self.getFranchises = function(){
        return $http.get(apiURL.FRANCHISE, {params : {limit : 1000}});
    };

    self.getFranchise = function(id){
        return $http.get(apiURL.FRANCHISE + '/' + id);
    };
    
    self.deleteFranchise = function(id){
        return $http.delete(apiURL.FRANCHISE + '/' + id);
    };

    self.enableFranchise = function(id){
        return $http.patch(apiURL.FRANCHISE + '/' + id);
    };

    self.saveFranchise = function(franquia){
        if(franquia._id){
            return $http.put(apiURL.FRANCHISE + '/' + franquia._id, franquia);
        } else{
            return $http.post(apiURL.FRANCHISE, franquia);
        }
    };

    /**
     * LOJAS *****
     */
    self.getStores = function(){
        return $http.get(apiURL.STORE, {params : {limit : 1000}});
    };

    self.getStoresByFranchise = function(franchiseId){
        return $http.get(apiURL.FRANCHISE_STORES(franchiseId), {params : {limit : 1000}});
    };

    self.getStore = function(id){
        return $http.get(apiURL.STORE + '/' + id);
    };

    self.deleteStore = function(id){
        return $http.delete(apiURL.STORE + '/' + id);
    };

    self.enableStore = function(id){
        return $http.patch(apiURL.STORE + '/' + id);
    };

    self.saveStore = function(store){
        var storeCopy = angular.copy(store);
        storeCopy.agent_json = JSON.stringify(store.agent);
        storeCopy.address_json = JSON.stringify(store.address);
        storeCopy.image = store.image;
        delete storeCopy.agent;
        delete storeCopy.address;
        if(storeCopy._id){
            return MultipartFormService.put(apiURL.STORE + '/' + store._id, storeCopy);
        } else{
            return MultipartFormService.post(apiURL.STORE, storeCopy);
        }
    };
    /**
     * SERVIÇOS LOJA *****
     */
    self.getStoreServices = function(storeId){
        return $http.get(apiURL.STORE_SERVICES(storeId), {params : {limit : 1000}});
    };

    self.getStoreService = function(storeId, serviceId){
        return $http.get(apiURL.STORE_SERVICES(storeId) + '/' + serviceId);
    };

    self.deleteStoreService= function(storeId, id){
        return $http.delete(apiURL.STORE_SERVICES(storeId) + '/' + id);
    };

    self.enableStoreService = function(storeId, id){
        return $http.patch(apiURL.STORE_SERVICES(storeId) + '/' + id, {}, {params : {status : true}});
    };

    self.disableStoreService= function(storeId, id){
        return $http.patch(apiURL.STORE_SERVICES(storeId) + '/' + id, {}, {params : {status : false}});
    };

    self.saveStoreService = function(storeId, storeService){
        var url = apiURL.STORE_SERVICES(storeId, storeService._id);
        if(storeService._id){
            return MultipartFormService.put(url, storeService);
        } else{
            return MultipartFormService.post(url, storeService);
        }
    };
    
    /**
     * LAVÁVEIS LOJA *****
     */
    self.getStoreWashables = function(storeId){
        return $http.get(apiURL.STORE_WASHABLES(storeId), {params : {limit : 1000}});
    };
    self.saveStoreWashables = function(storeId, washables){
        var deffered = $q.defer();
        validStoreWashables(washables).then(function(){
            $http.put(apiURL.STORE_WASHABLES(storeId), washables).then(function(result){
                deffered.resolve(result);
            }, function(error){
                deffered.reject(error);
            });
        }, function(error){
            deffered.reject(error);
        });
        return deffered.promise;
    };

    function validStoreWashables(washables){
        var deffered = $q.defer();
        var uniqueName = {};
        for (var i = 0; i < washables.length; i++) {
            var washable = washables[i];
            if(!washable.name){
                deffered.reject({status : 400, message : "O nome da peça é obrigatório, verifique se todas as peças tem o nome preenchido."}); 
            }
            if(washable.services_complete <= 0){
                deffered.reject({status : 400, message : "O valor do serviço completo deve ser maior que 0 para a peça " + washable.name+'.'}); 
            }
            if(uniqueName[washable.name]){
                deffered.reject({status : 400, message : "O nome da peça " + washable.name +' deve ser único.'}); 
            } else{
                uniqueName[washable.name] = true;
            }
        }
        deffered.resolve();
        return deffered.promise;
    }

    self.resetPassword = function (data) {
        return $http({
            method: 'PUT',
            url: apiURL.RESET_PASSWORD,
            headers: {
                'current-password': data.currentPassword,
                'new-password': data.newPassword
            }
        });
    };

}]);