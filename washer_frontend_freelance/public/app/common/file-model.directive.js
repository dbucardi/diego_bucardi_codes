angular.module("washerCommon").directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function () {
                scope.$apply(function () {
                    if(element[0].files[0]){
                        modelSetter(scope, element[0].files[0]);
                    } else {
                        // Restore the actual file uri if it exists when the user cancel filepicker
                        modelSetter(scope, scope.actualFileURI);
                    }
                });
            });

            scope.$watch(attrs.fileModel, function (fileModel) {
                // Keep the path to the actual file uri to restore if the user cancel filepicker
                if(fileModel && typeof fileModel === 'string'){
                    scope.actualFileURI = fileModel;
                }
            });
        }
    }
}]);

angular.module("washerApp").directive('imagePreview', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        scope: {
            fileModel : '='
        },
        link: function (scope, element, attrs) {
            checkImagePreview();
            var imageTag = '<img class="image-preview img-responsive" ng-click="openFilePicker()" src="{{imagePreview}}" title="Selecione o arquivo">';
            angular.element(element).before($compile(imageTag)(scope));
            angular.element(element).addClass('image-preview-hidden');
            element.bind('change', function () {
                readURL(this);
            });

            scope.$watch('fileModel', function(){
                checkImagePreview();
            });

            scope.openFilePicker = function(params) {
                angular.element(element).click();
            };

            function checkImagePreview(){
                scope.imagePreview = scope.fileModel ? scope.fileModel : 'assets/img/ic_picture.svg';
            }

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        scope.$apply(function () {
                            scope.imagePreview = e.target.result;
                        });
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        }
    }
}]);