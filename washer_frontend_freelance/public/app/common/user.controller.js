angular.module("washerCommon").controller('UserCtrl', function ($scope, ApiService, $window, authService) {
    $scope.logout = function(){
        authService.logout();
    };

    var loggedUser = authService.getUser();
    if(loggedUser){
        $scope.userName = loggedUser.name ? loggedUser.name : loggedUser.username;
        $scope.profile = loggedUser.attributes;
    } else{
        $scope.logout();
    }
});