angular.module("washerApp").controller('ListFranquiaCtrl', function ($scope, gridOptions, $location, ApiService, $filter) {
    initGridOptions();
    getFranchises();

    function initGridOptions() {
        $scope.gridOptions = angular.extend(angular.copy(gridOptions), {
            columnDefs: [
                { name: 'name', displayName: 'Franquia' },
                { name: 'social_reason', displayName: 'Razão Social' },
                { name: 'cnpj', displayName: 'CNPJ', cellFilter: 'mask: "99.999.999/9999-99"' },
                { 
                    name: 'enabled',
                    displayName: 'Status',
                    cellTemplate: "<div class='ui-grid-cell-contents'>{{row.entity.enabled ? 'Ativo' : 'Inativo'}}</div>"
                },
                {
                    name: 'edit', displayName: '',
                    width: 40, cellTemplate: 'app/franquia/actions.html'
                }
            ],
            onRegisterApi: function(gridApi){
                $scope.gridApi = gridApi;
              },
        });
    }

    function getFranchises() {
        $scope.loading = true;
        ApiService.getFranchises().then(function (response) {
            $scope.error = false;
            $scope.loading = false;
            $scope.data = response.data;
            $scope.filter();
        }, function (error) {
            $scope.loading = false;
            $scope.error = true;
        });
    }

    $scope.filter = function () {
        $scope.gridOptions.data = $filter('filter')($scope.data, $scope.filterValue);
        $scope.gridApi.grid.refresh();
    };

    $scope.onEditClick = function (entity) {
        $location.path('/franquias/' + entity._id);
    };
});