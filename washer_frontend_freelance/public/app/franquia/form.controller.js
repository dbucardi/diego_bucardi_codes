angular.module("washerApp").controller('FormFranquiaCtrl', function ($scope, gridOptions, $routeParams, $location, ApiService) {
    $scope.id = $routeParams.id;

    if ($scope.id === 'novo') {
        $scope.franquia = {};
    } else {
        $scope.loading = true;
        ApiService.getFranchise($scope.id).then(function (response) {
            $scope.loading = false;
            $scope.error = false;
            $scope.franquia = response.data;
        }, function (error) {
            $scope.loading = false;
            $scope.error = error.status;
        });
    }

    $scope.saveForm = function ($event) {
        $event.preventDefault();
        $scope.saveAction();
    };

    $scope.saveAction = function () {
        $scope.form.$submitted = true;
        if ($scope.form.$valid) {
            $scope.loading = true;
            ApiService.saveFranchise($scope.franquia).then(function (response) {
                $scope.loading = false;
                $scope.error = false;
                $scope.franquia = response.data;
                $location.path('/franquias')
            }, function (error) {
                $scope.loading = false;
                $scope.error = error.status;
                angular.element('html').scrollTop(0)
            });
        }
    };

    $scope.disableAction = function () {
        if(confirm("Tem certeza que deseja desabilitar este item?")){
            $scope.loading = true;
            ApiService.deleteFranchise($scope.franquia._id).then(function (response) {
                $scope.loading = false;
                $scope.error = false;
                $location.path('/franquias')
            }, function (error) {
                $scope.loading = false;
                $scope.error = error.status;
                angular.element('html').scrollTop(0)
            });
        }
    };

    $scope.enableAction = function () {
        $scope.loading = true;
        ApiService.enableFranchise($scope.franquia._id).then(function (response) {
            $scope.loading = false;
            $scope.error = false;
            $location.path('/franquias')
        }, function (error) {
            $scope.loading = false;
            $scope.error = error.status;
            angular.element('html').scrollTop(0)
        });
    };
});