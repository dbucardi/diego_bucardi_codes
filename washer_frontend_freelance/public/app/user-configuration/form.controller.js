angular.module("washerApp").controller('FormUserConfigCtrl', function ($scope, ApiService, $timeout) {
    $scope.reset = {};
    $scope.saveAction = function () {
        $scope.form.$submitted = true;
        if ($scope.form.$valid) {
            $scope.loading = true;
            ApiService.resetPassword($scope.reset).then(function (response) {
                $scope.loading = false;
                $scope.error = false;
                $scope.success = true;
                angular.element('html').scrollTop(0);
                $timeout(function(){
                    $scope.success = false;
                }, 3000);
            }, function (error) {
                $scope.loading = false;
                $scope.error = error.status;
                angular.element('html').scrollTop(0);
            });
        }
    };

    $scope.saveForm = function ($event) {
        $event.preventDefault();
        $scope.saveAction();
    };
});