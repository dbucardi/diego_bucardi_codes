angular.module("washerApp", 
['washerCommon', "ngRoute", "ui.grid", "ui.grid.pagination", 
'ui.bootstrap', 'appConfig', 'authConfig', 'ui.mask',
'ui.grid.edit', 'ui.grid.cellNav', 'ui.grid.selection']);