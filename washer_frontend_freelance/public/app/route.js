angular.module("washerApp").config(function($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    $routeProvider
    .when("/", {
        templateUrl : "app/inicio/inicio.html"
    })
    .when("/franquias", {
        templateUrl : "app/franquia/list.html",
        controller : "ListFranquiaCtrl"
    })
    .when("/franquias/:id", {
        templateUrl : "app/franquia/form.html",
        controller : "FormFranquiaCtrl"
    })
    .when("/lojas", {
        templateUrl : "app/loja/list.html",
        controller : "ListLojaCtrl"
    })
    .when("/lojas/:id", {
        templateUrl : "app/loja/form.html",
        controller : "FormLojaCtrl"
    })
    .when("/lojas/gerenciar-servicos/:id", {
        templateUrl: "app/loja-servicos/list.html",
        controller: "ListServicosLojaCtrl"
    })
    .when("/lojas/gerenciar-servicos/:id/:serviceId", {
        templateUrl: "app/loja-servicos/form.html",
        controller: "FormServicosLojaCtrl"
    })
    .when("/lojas/gerenciar-lavaveis/:id", {
        templateUrl: "app/loja-lavaveis/manage-washables.html",
        controller: "GerenciaLavaveisLojaCtrl"
    })
    .when("/user-configuration", {
        templateUrl: "app/user-configuration/form.html",
        controller: "FormUserConfigCtrl"
    })
    .otherwise({ redirectTo: '/' });
});