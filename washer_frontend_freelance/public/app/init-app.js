angular.module("washerApp").provider({
    $rootElement: function () {
        this.$get = function () {
            return angular.element('html');
        };
    }
});

angular.element(function () {
    var injector = angular.injector(['ng', 'washerApp', 'ngRoute']);
    var authService = injector.get('authService');
    var loggedUser = authService.getUser();
    var $location = injector.get('$location');
    if(authService.isLogged()){
        if($location.path() === '/' || $location.path() === ''){
            var menuItems = authService.getMenuItems();
            window.location.href = menuItems[0].location;
        }
        angular.element('body').removeClass('ng-hide');
        angular.bootstrap(document, ['washerApp']);
    } else{
        authService.logout();
    }
});
