angular.module("washerApp").controller('FormLojaCtrl', function ($scope, gridOptions, $routeParams, $location, ApiService, authService) {
    $scope.id = $routeParams.id;
    $scope.loja = {};
    //$scope.loja = {"name":"Loja teste 1","social_reason":"Loja teste 1","cnpj":"55555555555555","ie":"6666","franchise":{"_id":"59fcb246f1a40733cc52cdc4"},"address":{"place":"Rua teste","number":"334534","neighborhood":"Nova Campinas","city":"Campinas","state":"SP","postal_code":"13040111"},"agent":{"name":"Rogério Clio","email":"loja1@virtualcloset.com","cpf":"44444444444"}};
    init();

    function init(){
        if ($scope.id !== 'novo') {
            $scope.loading = true;
            ApiService.getStore($scope.id).then(function (response) {
                $scope.loading = false;
                $scope.error = false;
                $scope.loja = response.data;
                initPermitions();
            }, function (error) {
                $scope.loading = false;
                $scope.error = error.status;
            });
        } else{
            initPermitions();
        }
    }

    function initPermitions(){
        var loggedUser = authService.getUser();
        if(loggedUser.attributes === 'Administrator'){
            $scope.canEdit_services_qtd = true;
            getFranchises();
            $scope.canSelectFranchise = true;
        } else if(loggedUser.attributes === 'Franchise Manager'){
            $scope.canEdit_services_qtd = true;
            loadDefaultFrachise(loggedUser);
        }
    }

    function loadDefaultFrachise(loggedUser){
        $scope.franquias = [];
        $scope.franquias.push(loggedUser.franchise);
        $scope.loja.franchise = loggedUser.franchise._id;
        $scope.canSelectFranchise = false;
    }

    function getFranchises() {
        ApiService.getFranchises().then(function (response) {
            $scope.franquias = response.data;
        }, function (error) {

        });
    }

    $scope.saveForm = function ($event) {
        $event.preventDefault();
        $scope.saveAction();
    };

    $scope.saveAction = function () {
        $scope.form.$submitted = true;
        if ($scope.form.$valid) {
            $scope.loading = true;
            ApiService.saveStore($scope.loja).then(function (response) {
                $scope.loading = false;
                $scope.error = false;
                $scope.loja = response.data;
                $location.path('/lojas')
            }, function (error) {
                $scope.loading = false;
                $scope.error = error.status;
                angular.element('html').scrollTop(0)
            });
        }
    };

    $scope.disableAction = function () {
        if(confirm("Tem certeza que deseja desabilitar este item?")){
            $scope.loading = true;
            ApiService.deleteStore($scope.loja._id).then(function (response) {
                $scope.loading = false;
                $scope.error = false;
                $location.path('/lojas')
            }, function (error) {
                $scope.loading = false;
                $scope.error = error.status;
                angular.element('html').scrollTop(0)
            });
        }
    };

    $scope.enableAction = function () {
        $scope.loading = true;
        ApiService.enableStore($scope.loja._id).then(function (response) {
            $scope.loading = false;
            $scope.error = false;
            $location.path('/lojas')
        }, function (error) {
            $scope.loading = false;
            $scope.error = error.status;
            angular.element('html').scrollTop(0)
        });
    };
});