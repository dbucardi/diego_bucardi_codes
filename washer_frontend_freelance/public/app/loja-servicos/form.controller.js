angular.module("washerApp").controller('FormServicosLojaCtrl', function ($scope, gridOptions, $routeParams, $location, ApiService) {
    $scope.storeId = $routeParams.id;
    $scope.serviceId = $routeParams.serviceId;
    if ($scope.serviceId === 'novo') {
        $scope.service = {};
    } else {
        $scope.loading = true;
        ApiService.getStoreService($scope.storeId, $scope.serviceId).then(function (response) {
            $scope.loading = false;
            $scope.error = false;
            $scope.service = response.data;
        }, function (error) {
            $scope.loading = false;
            $scope.error = error.status;
            $scope.service = {};
        });
    }
    
    $scope.onBackClick = function(){
        $location.path('/lojas/gerenciar-servicos/' + $scope.storeId);
    };

    $scope.saveForm = function ($event) {
        $event.preventDefault();
        $scope.saveAction();
    };

    $scope.saveAction = function () {
        $scope.form.$submitted = true;
        if ($scope.form.$valid) {
            $scope.loading = true;
            ApiService.saveStoreService($scope.storeId, $scope.service).then(function (response) {
                $scope.loading = false;
                $scope.error = false;
                $location.path('/lojas/gerenciar-servicos/' + $scope.storeId);
            }, function (error) {
                $scope.loading = false;
                $scope.error = error.status;
                angular.element('html').scrollTop(0);
            });
        }
    };

    $scope.disableAction = function () {
        if(confirm("Tem certeza que deseja desabilitar este item?")){
            $scope.loading = true;
            ApiService.disableStoreService($scope.storeId, $scope.service._id).then(function (response) {
                $scope.loading = false;
                $scope.error = false;
                $location.path('/lojas/gerenciar-servicos/' + $scope.storeId);
            }, function (error) {
                $scope.loading = false;
                $scope.error = error.status;
                angular.element('html').scrollTop(0)
            });
        }
    };

    $scope.enableAction = function () {
        $scope.loading = true;
        ApiService.enableStoreService($scope.storeId, $scope.service._id).then(function (response) {
            $scope.loading = false;
            $scope.error = false;
            $location.path('/lojas/gerenciar-servicos/' + $scope.storeId);
        }, function (error) {
            $scope.loading = false;
            $scope.error = error.status;
            angular.element('html').scrollTop(0)
        });
    };
});