angular.module("washerApp").controller('ListServicosLojaCtrl', function ($scope, gridOptions, $location, ApiService, $filter, $routeParams) {
    $scope.storeId = $routeParams.id;
    initGridOptions();
    getStoreServices();

    function initGridOptions() {
        $scope.gridOptions = angular.extend(angular.copy(gridOptions), {
            columnDefs: [
                { name: 'name', displayName: 'Nome do serviço' },
                { name: 'price', displayName: 'Preço', cellFilter: 'currency : "R$" : 2' },
                { 
                    name: 'enabled',
                    displayName: 'Status',
                    cellTemplate: "<div class='ui-grid-cell-contents'>{{row.entity.enabled ? 'Ativo' : 'Inativo'}}</div>"
                },
                {
                    name: 'edit', displayName: '',
                    width: 40, cellTemplate: 'app/loja-servicos/actions.html'
                }
            ],
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        });
    }

    function getStoreServices() {
        $scope.loading = true;
        ApiService.getStoreServices($scope.storeId).then(function (response) {
            $scope.error = false;
            $scope.loading = false;
            $scope.data = response.data;
            $scope.filter();
        }, function (error) {
            $scope.loading = false;
            $scope.error = true;
        });
    }

    $scope.filter = function () {
        $scope.gridOptions.data = $filter('filter')($scope.data, $scope.filterValue);
        $scope.gridApi.grid.refresh();
    };

    $scope.onEditClick = function (entity) {
        $location.path('/lojas/gerenciar-servicos/' + $scope.storeId + '/' + entity._id);
    };

    $scope.onServiceClick = function (entity) {
        $location.path('/lojas/gerenciar-servicos/' + entity._id);
    };

    $scope.onWashableClick = function (entity) {
        $location.path('/lojas/gerenciar-lavaveis/' + entity._id);
    };
});