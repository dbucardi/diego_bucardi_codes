angular.module("loginApp", ['authConfig', 'appConfig']);
angular.module("loginApp").controller("loginController", function ($scope, $window, LoginService, authService) {
    /**
     * Possible keys: 
     * showLogin, showResetPassword, showResetPasswordResult
     */
    $scope.showPage = function(pageKey){
        $scope.showLogin = false;
        $scope.showResetPassword = false;
        $scope.showResetPasswordResult = false;
        $scope[pageKey] = true;
    };
    $scope.showPage('showLogin');

    $scope.login = function ($event) {
        $event.preventDefault();
        $scope.formLogin.$submitted = true;
        if ($scope.formLogin.$valid) {
            $scope.loading = true;
            LoginService.login({
                username: $scope.username,
                password: $scope.password
            }).then(function (response) {
                $scope.loading = false;
                authService.saveAuthorization(response.data);
                $window.location.href = '/';
            }, function (error) {
                $scope.loading = false;
                $scope.invalidCredentials = true;
            });
        }
    };

    $scope.resetPassword = function ($event) {
        $event.preventDefault();
        $scope.resetForm.$submitted = true;
        if ($scope.resetForm.$valid) {
            $scope.loading = true;
            LoginService.resetPassword({
                username: $scope.username
            }).then(function () {
                $scope.loading = false;
                $scope.showPage('showResetPasswordResult');
            }, function (error) {
                $scope.loading = false;
                $scope.error = error.status;
                $scope.showPage('showResetPasswordResult');
            });
        }
    };
});