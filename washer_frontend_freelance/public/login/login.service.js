angular.module("loginApp").service("LoginService", function ($http, apiURL) {
    var self = this;
    self.login = function (data) {
        return $http({
            method: 'GET',
            url: apiURL.LOGIN,
            headers: {
                email: data.username,
                password: data.password
            }
        });
    };

    self.resetPassword = function (data) {
        return $http({
            method: 'GET',
            url: apiURL.FORGOT_PASSWORD,
            params: {
                email: data.username,
                goal: 'Agent'
            }
        });
    };
});