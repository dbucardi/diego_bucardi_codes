angular.module("authConfig", []);
angular.module("authConfig").service('authService', function ($window) {
    var self = this;
    self.saveAuthorization = function (response) {
        $window.sessionStorage.setItem('accessToken', response.jwt_token);
        $window.sessionStorage.setItem('user', JSON.stringify(response.data));
    };

    self.getUser = function () {
        var strData = $window.sessionStorage.getItem('user');
        if (strData) {
            return JSON.parse(strData);
        }
        return null;
    };

    self.isLogged = function () {
        var loggedUser = self.getUser();
        return  (loggedUser
            && loggedUser.attributes
            && $window.sessionStorage.accessToken) ? true : false;
    };

    self.logout = function () {
        $window.sessionStorage.accessToken = '';
        $window.sessionStorage.user = '';
        $window.location.href = '/login';
    };

    self.getMenuItems = function () {
        var menus = [], loggedUser = self.getUser();
        switch (loggedUser.attributes) {
            case 'Administrator':
                menus.push({
                    title: 'FRANQUIAS',
                    location: '#/franquias',
                    section: 'franquias'
                });
                menus.push({
                    title: 'LOJAS',
                    location: '#/lojas',
                    section: 'lojas'
                });
                break;

            case 'Franchise Manager':
                menus.push({
                    title: 'LOJAS',
                    location: '#/lojas',
                    section: 'lojas'
                });
                break;

            case 'Store Manager':
                menus.push({
                    title: 'PEÇAS',
                    location: '#/lojas/gerenciar-lavaveis/' + loggedUser.store._id,
                    section: 'gerenciar-lavaveis'
                });
                menus.push({
                    title: 'SERVIÇOS',
                    location: '#/lojas/gerenciar-servicos/' + loggedUser.store._id,
                    section: 'gerenciar-servicos'
                });
                break;

            default:
                menus.push({ title: 'HOME', location: '#/' });
                break;
        }
        menus.push({ title: 'CONFIGURAÇÕES', location: '#/user-configuration', section: 'user-configuration' });
        return menus;
    };
});