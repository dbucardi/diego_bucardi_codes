angular.module("appConfig", []);
angular.module("appConfig").constant('gridOptions', {
    paginationPageSize: 10,
    enablePaging: true,
    enablePaginationControls: true,
    paginationPageSizes: [10, 25, 50],
    enableColumnMenus: false,
    rowHeight : 40,
    headerRowHeight  : (44 + 50),
    enableVerticalScrollbar: 0,
    enableHorizontalScrollbar: 0,
    enablePaginationControls: false
});

angular.module("appConfig").factory('apiURL', function(){
    var SERVER = '/aV';
    return {
        LOGIN : SERVER + '/login',
        RESET_PASSWORD : SERVER + '/reset_password',
        FORGOT_PASSWORD : SERVER + '/forgot_password',
        FORGOT_PASSWORD_RESET : SERVER + '/reset_password',
        FRANCHISE : SERVER + '/franchise',
        FRANCHISE_STORES : function(franchiseId){
            return SERVER + '/franchise/' + franchiseId + '/stores';
        },
        STORE : SERVER + '/store',
        STORE_SERVICES : function(storeId, serviceId){
            return SERVER + '/store/' + storeId + '/services' + (serviceId ? '/' + serviceId : '');
        },
        STORE_WASHABLES : function(storeId){ 
            return SERVER + '/store/' + storeId + '/washables';
        }
    };
});