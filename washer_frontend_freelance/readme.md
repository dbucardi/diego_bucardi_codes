# Washer Frontend

## Como rodar o projeto pela primeira vez em desenvolvimento

- Verifique se o node está instalado digitando 'npm' o prompt de comando
- Entre na pasta raiz do projeto frontend
- Execute o comando 'npm install --global-style'
- Instale o gulp 'npm install -g gulp'
- Execute o comando 'gulp serve' para iniciar a aplicação
- Endereço de login: http://localhost:3000/login/
- Usuário: dibu@teste.com senha: 123456

## Como gerar a build de produção

- Entre na pasta raiz do projeto frontend
- Execute o comando 'npm install --global-style'
- Execute o comando 'gulp build', os arquivos serão gerados na pasta dist

#usuarios de teste
franquia@virtualcloset.com "Franchise Manager"
admin@virtualcloset.com "Administrator"
loja@virtualcloset.com "Store Manager" 