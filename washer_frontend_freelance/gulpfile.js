var gulp = require('gulp')
sass = require('gulp-sass')
del = require('del')
runSequence = require('run-sequence')
browserSync = require('browser-sync').create()
usemin = require('gulp-usemin');

var release_dir = '../../release/frontend-dist/';

gulp.task('sass', function() {
    return gulp.src('public/assets/scss/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('public/assets/css'))
        .pipe(browserSync.reload({
            stream: true
        }))
})

gulp.task('usemin', function() {
    return gulp.src('public/index.html')
        .pipe(usemin())
        .pipe(gulp.dest(release_dir));
});

gulp.task('usemin-login', function() {
    return gulp.src('public/login/index.html')
        .pipe(usemin())
        .pipe(gulp.dest(release_dir + 'login/'));
});

gulp.task('clean:dist', function() {
    return del.sync('../dist', {force:true})
});

gulp.task('clean:dist-node_modules', function() {
    return del.sync('../dist/node_modules/**/*.js', {force:true})
})

gulp.task('copyhtml', function() {
    return gulp.src('public/**/*.html')
        .pipe(gulp.dest(release_dir));
});

gulp.task('copyimages', function() {
    gulp.src('public/**/*.svg')
        .pipe(gulp.dest(release_dir));
    gulp.src('public/**/*.gif')
        .pipe(gulp.dest(release_dir));
});

gulp.task('copyfonts', function() {
    gulp.src('node_modules/bootstrap/dist/fonts/*')
        .pipe(gulp.dest(release_dir + 'fonts'));
    gulp.src('node_modules/angular-ui-grid/*.eot')
        .pipe(gulp.dest(release_dir + 'css/'));
    gulp.src('node_modules/angular-ui-grid/*.ttf')
        .pipe(gulp.dest(release_dir + 'css/'));
    gulp.src('node_modules/angular-ui-grid/*.woff')
        .pipe(gulp.dest(release_dir + 'css/'));
});

gulp.task('watch', ['browser-sync-src', 'sass'], function() {
    gulp.watch('public/assets/scss/**/*.scss', ['sass', browserSync.reload]);
    gulp.watch('public/app/**/*.html', browserSync.reload);
    gulp.watch('public/login/**/*.html', browserSync.reload);
    gulp.watch('public/app/**/*.js', browserSync.reload);
    gulp.watch('public/login/**/*.js', browserSync.reload);
})

gulp.task('build', function(callback) {
    runSequence('clean:dist', ['sass', 'usemin', 'copyhtml', 'usemin-login', 'copyfonts', 'copyimages', 'clean:dist-node_modules'],
        callback
    )
})

gulp.task('browser-sync-dist', function() {
    browserSync.init({
        server: {
            baseDir: "../dist"
        }
    });
});

gulp.task('browser-sync-src', function() {
    browserSync.init({
        server: {
            baseDir: "public/"
        }
    });
});

/**
 * Workaround used during development to make node_modules folder available.
 */
gulp.task('copy-modules', function() {
    gulp.src('node_modules/**/*')
    .pipe(gulp.dest('public/node_modules/'));
});

/**
 * Serving Development Files
 */
gulp.task('serve', ['sass', 'copy-modules', 'watch']);

/**
 * Serving Production Files
 */
gulp.task('serve:dist', ['build', 'browser-sync-dist']);