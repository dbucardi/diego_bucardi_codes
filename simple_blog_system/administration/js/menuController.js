
siteApp.controller("AdminMenuCtrl", function($scope, $http, $routeParams, $location, siteAdminService) {
  $scope.init = function(){
    $scope.menus = [];
    siteAdminService.getMenuTree().success(function(data) {
      if(data){
        $scope.menus = data;
      }
    });
  };

  $scope.addChild = function(menu){
    var menuToAdd = menu ? menu.menus : $scope.menus;
    menuToAdd.push({
      title : "",
      group : "",
      url : "",
      menus : []
    });
  };


  $scope.deleteChild = function(parent, item){
    deleteMenuByReference(parent, item);
  };

  $scope.save = function(){
    siteAdminService.saveMenu($scope.menus).success(function(data) {
        alert("Salvou com sucesso");
    });
  };

  function deleteMenuByReference(parent, item){
    for(var i = 0;i < parent.length; i++){
      var itemToCompare = parent[i];
      if(itemToCompare == item){
        parent.splice(i, 1);
        return true;
      }
    }
    return undefined;
  };

  $scope.init();
});
