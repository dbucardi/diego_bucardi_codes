
siteApp.controller("AdminListCtrl", function($scope, $http, $location, $cookies, siteAdminService) {
  $scope.init = function(){
    if($cookies.session_id == undefined){
      $location.path('/login');
      return;
    }
    $scope.getPosts();
  };

  $scope.getPosts = function(){
    siteAdminService.getPosts().success(function(data) {
      $scope.posts = data;
    });
  };

  $scope.deletePost = function(post){
    var confimation = confirm("Remover?");
    if(confimation){
      siteAdminService.deletePost(post.id).success(function(data) {
        $scope.posts.splice($scope.posts.indexOf(post), 1);
      });
    }
  };

  $scope.saveOrder = function(){
    siteAdminService.savePostsOrder($scope.posts).success(function(data) {
      $scope.getPosts();
    });
  };

  $scope.init();
});

siteApp.controller("AdminCreateCtrl", function($scope, $http, $routeParams, $location, $cookies, FileUploader, siteAdminService) {

  $scope.init = function(){
    if($cookies.session_id == undefined){
      $location.path('/login');
      return;
    }
    $scope.post = {};
    $scope.itemId =  $routeParams.itemId;
    if($scope.itemId == undefined){
      $scope.post = {"id" : null,
      "title" : "",
      "thumb" : "",
      "category_id" : 1,
      "body" : "",
      "small_desc" : "",
      "date_string" : "",
      "menu_group" : "",
      "content_type" : "",
      "tags" : []};
    } else{
      siteAdminService.getPostById($scope.itemId).success(function(data) {
        $scope.post = data;
        if(!$scope.post.tags){
          $scope.post.tags = [];
        }
      });
    }

    siteAdminService.getContentTypes().then(function(contentTypes){
      $scope.contentTypes = contentTypes;
    });

    siteAdminService.getMenus().then(function(menus){
      $scope.menus = menus;
    });
  };

  $scope.save = function(){
    siteAdminService.savePost($scope.post, $scope.itemId).success(function(data) {
      $location.path('/');
    });
  };

  $scope.addTag = function(){
    if($scope.tagName && $scope.post.tags.indexOf($scope.tagName) < 0){
      $scope.post.tags.push($scope.tagName);
      $scope.tagName = "";
    }
  };

  $scope.removeTag = function(tag){
    var index = $scope.post.tags.indexOf(tag);
    $scope.post.tags.splice(index, 1);
  };

  $scope.uploader = new FileUploader({
    url: 'img/upload-post.php',
    autoUpload  : true
  });
  $scope.thumbUploader = new FileUploader({
    url: 'img/upload-thumb.php',
    autoUpload  : true
  });
  $scope.uploader.filters.push({
    name: 'imageFilter',
    fn: function(item /*{File|FileLikeObject}*/, options) {
      var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
      return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }
  });

  $scope.thumbUploader.onCompleteItem = function(fileItem, response, status, headers) {
    $scope.post.thumb = "img/posts/" + response;
  };

  $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
    fileItem.file.name = response;
  };

  $scope.init();
});
