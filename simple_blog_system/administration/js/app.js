var siteApp = angular.module("siteApp", ['ngRoute', 'ngAnimate', 'ui.bootstrap', 'ui.bootstrap.carousel','ngSanitize', 'ngCookies', 'angularFileUpload', 'textAngular']);

siteApp.filter('urlformat', function () {
    return function (value) {
        return (!value) ? '' : value.replace(/ /g, '-');
    };
});

siteApp.filter('filterPortifolioByCategory', function () {
    return function(input, category) {
        if(input != undefined && category > 0){
	    var out = [];
	    for (var i = 0; i < input.length; i++){
	        if(input[i].category_id == category)
	        out.push(input[i]);
	    }
	    return out;
        }else{
            return input;
        }
    };
});

function getQueryString(queryName) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == queryName) {
            return decodeURIComponent(pair[1]);
        }
    }
	return undefined;
}

function ga(){

}
