siteApp.config(['$routeProvider','$locationProvider',
    function($routeProvider, $locationProvider) {
        $routeProvider.
        when('/', {
            templateUrl: 'partials/admin.html',
            controller: 'AdminListCtrl'
        }).
        when('/login', {
            templateUrl: 'partials/admin_login.html',
            controller: 'AdminLoginCtrl'
        }).
        when('/create/', {
            templateUrl: 'partials/admin_create.html',
            controller: 'AdminCreateCtrl'
        }).
        when('/create/:itemId', {
            templateUrl: 'partials/admin_create.html',
            controller: 'AdminCreateCtrl'
        }).
        when('/menu', {
            templateUrl: 'partials/menu_create.html',
            controller: 'AdminMenuCtrl'
        }).
        otherwise({
            redirectTo: '/'
        });
		$locationProvider.hashPrefix('!');
    }
]);
