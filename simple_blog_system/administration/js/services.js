siteApp.service('siteAdminService', function ($http, $q) {
    this.getPosts = function(){
      return $http.get('data/rest.php?type=post', {cache: true});
    };

    this.deletePost = function(itemId){
      return $http({
          method: 'DELETE',
          url: 'data/rest.php?type=post&id=' + itemId,
          headers: {'Content-Type': 'application/json'}
      });
    };

    this.getPostById = function(itemId){
      return $http({
        method: 'GET',
        url: 'data/rest.php?type=post&id=' + itemId
      });
    };

    this.savePost = function(post, itemId){
      var url = 'data/rest.php?type=post';
      var method = 'POST';
      if(itemId != undefined){
        url += '&id=' + itemId;
        url += '&type=update_post';
        method = 'PUT';
      }
      return $http({
        method: method,
        url: url,
        data : post,
        headers: {'Content-Type': 'application/json'}
      });
    };

    this.savePostsOrder = function(posts){
      return $http({
          method: 'PUT',
          url: 'data/rest.php?type=update_position',
          data : posts,
          headers: {'Content-Type': 'application/json'}
      });
    };

    this.getContentTypes = function(){
      var deferred = $q.defer();
      var contentTypes = [{title : "Destino", value : "destino"}, {title : "Pagina", value : "pagina"}];
      deferred.resolve(contentTypes);
      return deferred.promise;
    };

    this.getMenus = function(){
      var deferred = $q.defer();
      $http({
        method: 'GET',
        url: '../front/cache/menu.json'
      }).success(function(data) {
        var groups = [];
        var menu1;
        var menu2;
        var menu3;
        for(var i = 0; i < data.length; i++){
          menu1 = data[i];
          if(menu1.group) { groups.push(menu1); }
          if(menu1.menus){
            for(var j = 0; j < menu1.menus.length; j++){
              menu2 = menu1.menus[j];
              if(menu2.group) { groups.push(menu2); }
              if(menu2.menus){
                for(var k = 0; k < menu2.menus.length; k++){
                  menu3 = menu2.menus[k];
                  if(menu3.group) { groups.push(menu3); }
                }
              }
            }
          }
        }
        deferred.resolve(groups);
      });
      return deferred.promise;
    };

    this.getMenuTree = function(){
      return $http.get("../front/cache/menu.json");
    };

    this.saveMenu = function(menus){
      return $http({
          method: "POST",
          url: "data/saveMenu.php",
          data : menus,
          headers: {'Content-Type': 'application/json'}
      });
    };
});
