siteApp.controller("AdminLoginCtrl", ['$scope', '$http', '$routeParams', '$location', '$cookies', function($scope, $http, $routeParams, $location, $cookies) {
 	if($cookies.session_id != undefined){
 		$location.path('/');
 		return;
 	}

 	$scope.loginData = {"user" : "", "pass" : ""};
 	$scope.error = "";
	$scope.login = function(){
		$scope.error = "";
		$http({
			method: 'POST',
			url: 'data/login.php',
			data : $scope.loginData,
			headers: {'Content-Type': 'application/json'}
		}).success(function(data) {
			if(data.success!= undefined){
				$cookies.session_id = data.token;
				location.reload();
			} else if(data.error != ""){
				$scope.error = data.error;
			}
		});
	};
}]);
