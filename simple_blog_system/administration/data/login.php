<?php
require("utils.php");
header('Content-type: application/json');
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
$data = json_decode(file_get_contents("php://input"));
if($data){
	if($data->user == "ser@serviajante.com" && $data->pass == "123456"){
		global $privateCode;
		$expire=time()+60*60*24*365;
		setcookie("session_id", $privateCode, $expire, '/', $_SERVER['SERVER_NAME']);
		
		echo json_encode(array('success'=>true, 'token'=>$privateCode));
	} else{
		echo json_encode(array('error'=>'Invalid user name or password'));
	}
} else{
	echo json_encode(array('error'=>'No data found'));
}
?>