<?php
$connection = false;

function getSiteConnection(){
    error_reporting(1);
    global $connection;
    logMessage("begin getSiteConnection");
    logMessage("start creating connection");
    $options = array(
      PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );
    $connection = mysql_connect('localhost', 'root', '');
    logMessage("end creating connection");
    if (!$connection) {
        die('Não foi possível conectar: ' . mysql_error());
    }
    logMessage("start selecting db");
    mysql_select_db('serviajante', $connection) or die('Could not select database.');
    logMessage("end selecting db");
    return $connection;
}

function closeSiteConnnection()
{
    global $connection;
    if($connection != false){
        mysql_close($connection);
        logMessage("closing connection");
    }
    $connection = false;
}

function db_select($sql){
    $res = mysql_query($sql, getSiteConnection());
    $rows = array();
    if($res){
        while($r = mysql_fetch_assoc($res)) {
            $rows[] = array_map('utf8_encode', $r);
        }
    } else{
        return json_encode(array('error' => mysql_error()));
    }
    return json_encode($rows);
}

function db_select_array($sql){
    $res = mysql_query($sql, getSiteConnection());
    $rows = array();
    if($res){
        while($r = mysql_fetch_assoc($res)) {
            $rows[] = array_map('utf8_encode', $r);
        }
    } else{
        return json_encode(array('error' => mysql_error()));
    }
    return $rows;
}

function db_execute($sql){
    $connection = getSiteConnection();
    mysql_query("SET NAMES utf8", $connection);
    //mysql_query("SET CHARACTER SET 'utf8_unicode_ci'", $connection);
    $res = mysql_query($sql, $connection) or die(mysql_error());
    mysql_query("SET NAMES latin1", $connection);
    if($res){
        return json_encode(array("result" => "ok"));
    }else{
        return json_encode(array('error' => mysql_error()));
    }
}

?>
