<?php
error_reporting(E_ERROR | E_PARSE);
require_once 'sdk-1.6.2/sdk.class.php';
define('AWS_KEY', '');
define('AWS_SECRET_KEY', '');
define('AWS_CANONICAL_ID', '');
define('AWS_CANONICAL_NAME', '');
define('AWS_S3_BUCKET', '');

function getPosts($limit = null){
    $sql = "SELECT * from post p ";
    $sql .= " order by position ";
    if(!is_null($limit)){
        $sql .= "limit 0, ".$limit;
    }

    $rows = db_select_array($sql);
    foreach ($rows as $key => $row) {
      $rows[$key]["tags"] = json_decode($row["tags"]);
    }

    return json_encode($rows);
}

function getTags(){
  $sql = "SELECT tags from post p ";
  $rows = db_select_array($sql);
  $tags = array();
  foreach ($rows as $key => $row) {
    $tagArray = json_decode($row["tags"]);
    if($tagArray){
      foreach ($tagArray as $key2 => $tag) {
        if (!in_array($tag, $tags)) {
          $tags[] = $tag;
        }
      }
    }
  }
  return json_encode($tags);
}

function getDestinos(){
    return db_select("SELECT distinct menu_group FROM post");
}

function getPostById($id){
    $sql = "SELECT * from post p ";
    if(!is_null($id))
        $sql .= "where p.id = $id";

    $rows = db_select_array($sql);
    $rows[0]["tags"] = json_decode($rows[0]["tags"]);
    return json_encode($rows[0]);
}

function insertPost($post){
	$today = date("Y-m-d");
    $sql = "insert into post(title, thumb, last_update, body, small_desc, date_string, content_type, menu_group, position) ";
    $sql .= "values(";
    $sql .= "'".$post->title."'";
    $sql .= ", '".$post->thumb."'";
    $sql .= ", '".$today."'";
    $sql .= ", '".$post->body."'";
    $sql .= ", '".$post->small_desc."'";
    $sql .= ", '".$post->date_string."'";
    $sql .= ", '".$post->content_type."'";
    $sql .= ", '".$post->menu_group."'";
    if($post->tags){
      $sql .= ", '".json_encode($post->tags)."'";
    }
    $sql .= ", '".$post->tags."'";
    $sql .= ")";
    return db_execute($sql);
}

function deletePost($id){
    $sql = "delete from post ";
    $sql .= "where id =". $id;
    return db_execute($sql);
}

function updatePost($post){
	$today = date("Y-m-d");
    $sql = "update post set ";
    $sql .= "title = '".$post->title."'";
    $sql .= ", thumb = '".$post->thumb."'";
	$sql .= ", last_update = '".$today."'";
    $sql .= ", body = '".$post->body."'";
    $sql .= ", small_desc = '".$post->small_desc."'";
    $sql .= ", date_string = '".$post->date_string."'";
    $sql .= ", content_type = '".$post->content_type."'";
    $sql .= ", menu_group = '".$post->menu_group."'";
    if($post->tags){
      $sql .= ", tags = '".json_encode($post->tags)."'";
    }
    $sql .= " where id =".$post->id;
    return db_execute($sql);
}

function getLastPostId(){
	$lastId = db_select_array('select max(id) as max_id from post');
	return $lastId[0]['max_id'];
}

function updatePostPositions($posts){
    $sql = '';
    logMessage('start method updatePostPositions');
    foreach($posts as $post){
    	if(isset($post->position)){
            $sql = "update post set ";
            $sql .= "position = ".$post->position;
            $sql .= " where id =".$post->id;
            logMessage('start executing sql');
            db_execute($sql);
            logMessage( "end executing sql");
        }
    }
    logMessage('end method updatePostPositions');
}

function postExists($id){
	$postId = db_select_array('select id from post where post.id = '.$id);
    return count($postId) > 0;
}

function saveCacheFiles($id = null){
	$s3 = createS3Connection();
	if(!is_null($id)){
		//unlink
		if(postExists($id)){
			$post = getPostById($id);
			file_put_contents('../../front/cache/posts/'.$id.'.json', $post);
			saveFileAmazonS3($s3, 'cache/posts/'.$id.'.json', $post);
		} else{
			unlink('../../front/cache/posts/'.$id.'.json');
			deleteFileAmazonS3($s3, 'cache/posts/'.$id.'.json');
		}
	}
	$allPosts = getPosts();
	$frontPosts = getPosts(6);
	$destinos = getDestinos();
	$postsMap = generatePostsSiteMap();
    $tags = getTags();

	//Save local
	file_put_contents('../../front/cache/posts.json', $allPosts);
	file_put_contents('../../front/cache/posts_front.json',  $frontPosts);
	file_put_contents('../../front/cache/destinos.json', $destinos);
    file_put_contents('../../front/cache/tags.json', $tags);
	file_put_contents('../../front/posts-sitemap.xml', $postsMap);

	//Save into S3
	saveFileAmazonS3($s3, 'cache/posts.json', $allPosts);
	saveFileAmazonS3($s3, 'cache/posts_front.json', $frontPosts);
	saveFileAmazonS3($s3, 'cache/destinos.json', $destinos);
    saveFileAmazonS3($s3, 'cache/tags.json', $tags);
	saveFileAmazonS3($s3, 'cache/menu.json', file_get_contents('../../front/cache/menu.json'));
	saveFileAmazonS3($s3, 'posts-sitemap', $postsMap );
}


function xml_encode($mixed, $domElement=null, $DOMDocument=null) {
    if (is_null($DOMDocument)) {
        $DOMDocument =new DOMDocument;
        $DOMDocument->formatOutput = true;
        xml_encode($mixed, $DOMDocument, $DOMDocument);
        echo $DOMDocument->saveXML();
    }
    else {
        if (is_array($mixed)) {
            foreach ($mixed as $index => $mixedElement) {
                if (is_int($index)) {
                    if ($index === 0) {
                        $node = $domElement;
                    }
                    else {
                        $node = $DOMDocument->createElement($domElement->tagName);
                        $domElement->parentNode->appendChild($node);
                    }
                }
                else {
                    $plural = $DOMDocument->createElement($index);
                    $domElement->appendChild($plural);
                    $node = $plural;
                    if (!(rtrim($index, 's') === $index)) {
                        $singular = $DOMDocument->createElement(rtrim($index, 's'));
                        $plural->appendChild($singular);
                        $node = $singular;
                    }
                }

                xml_encode($mixedElement, $node, $DOMDocument);
            }
        }
        else {
            $domElement->appendChild($DOMDocument->createTextNode($mixed));
        }
    }
}

function generatePostsSiteMap(){
	$posts = getPosts();
	$data = array();

	foreach ($posts as $post) {
		$url = array(
			'loc' => 'http://www.ptambalointeriores.com.br/#!/portifolio/'.$post['id'],
			'lastmod' => '2015-01-15T15:00:08+00:00',
			'changefreq' => 'weekly',
			'priority' => '0.8',
			'image:image' => array('image:loc' => 'http://www.ptambalointeriores.com.br/'.$post['thumb'], 'image:caption' => $post['title']),
		);
		if(!is_null($post['last_update'])){
			$url['lastmod'] = $post['last_update'].'T15:00:08+00:00';
		}
		$data['url'][] = $url;
	}

	$DOMDocument = new DOMDocument;
	$DOMDocument->formatOutput = true;
	$parentNode = $DOMDocument->createElement('urlset');
	$parentNode->setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
	$parentNode->setAttribute('xmlns:image', 'http://www.google.com/schemas/sitemap-image/1.1');
	$parentNode->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
	$parentNode->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
	$DOMDocument->appendChild($parentNode);
	xml_encode($data, $parentNode, $DOMDocument);
	return $DOMDocument->saveXML();
}

function saveFileAmazonS3($Connection, $filePath, $fileBody){
  return $Connection->create_object(AWS_S3_BUCKET, $filePath, array(
			'body' => $fileBody,
			'acl' => AmazonS3::ACL_PUBLIC
	));
}

function deleteFileAmazonS3($Connection, $filePath){
	return $Connection->delete_object(AWS_S3_BUCKET, $filePath);
}

function createS3Connection(){
	// Instantiate the S3 class and point it at the desired host
    $Connection = new AmazonS3(array(
		'key'            => AWS_KEY,
		'secret'         => AWS_SECRET_KEY,
		'canonical_id'   => AWS_CANONICAL_ID,
		'canonical_name' => AWS_CANONICAL_NAME,
	));
	$Connection->disable_ssl_verification();
	$Connection->allow_hostname_override(false);
	return $Connection;
}

?>
