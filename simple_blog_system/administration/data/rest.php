<?php
require("utils.php");
require("database.php");
require("methods.php");
require("handleRequest.php");

switch ($method) {
  case 'PUT':
    if($session_id == $privateCode){
    	if($type == "update_position"){
    	    updatePostPositions($data);
			saveCacheFiles();
    	} else if($type == "update_post"){
    	    updatePost($data);
			saveCacheFiles($data->id);
    	}
    }else{
    	header("HTTP/1.0 403 Forbidden");
    }
    break;
  case 'POST':
    if($session_id == $privateCode){
    	insertPost($data);
		$last_id = getLastPostId();
		saveCacheFiles($last_id);
    }else{
    	header("HTTP/1.0 403 Forbidden");
    }
    break;
  case 'GET':
    if(is_null($id)){
      echo getPosts($limit);
    } else{
      echo getPostById($id);
    }
    break;
  case 'DELETE':
    if($session_id == $privateCode){
	     if($type == "post"){
         deletePost($id);
			   saveCacheFiles($id);
    	}
    } else{
	     header("HTTP/1.0 403 Forbidden");
    }
    break;
  default:
    echo '';
    break;
}
closeSiteConnnection();

?>
