<?php
    header('Content-type: application/json');
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

    $method = $_SERVER['REQUEST_METHOD'];
    $type = isset($_GET["type"]) ? $_GET["type"] : null ;
    $id = isset($_GET["id"]) ?  $_GET["id"] : null;
    $limit = isset($_GET["limit"]) ?  $_GET["limit"] : null;
    $content_type = isset($_GET["content_type"]) ?  $_GET["content_type"] : null;
    logMessage("start converting json");
    $data = json_decode(file_get_contents("php://input"));
    logMessage("end convertiing json");
    global $privateCode;
    $session_id = $_COOKIE["session_id"];
?>
