<?php
include("resize-class.php");
require_once '../data/utils.php';
require_once '../data/database.php';
require_once '../data/methods.php';

if ( !empty( $_FILES ) ) {
  $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];
  $uploadPath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'posts' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];
	$uploadPathOriginal = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'posts' . DIRECTORY_SEPARATOR . 'original' . DIRECTORY_SEPARATOR . $_FILES[ 'file' ][ 'name' ];
  move_uploaded_file($tempPath, $uploadPathOriginal);
	$resizeObj = new resize($uploadPathOriginal);
	$resizeObj -> resizeImage(960, 600);
	$uploadPath = str_replace('.png', '.jpg', $uploadPath);
	$resizeObj -> saveImage($uploadPath, 85);
	$imgName = str_replace('.png', '.jpg', $_FILES[ 'file' ][ 'name' ]);

  $uploadPathFront = '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR .'front'. DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . 'posts' . DIRECTORY_SEPARATOR . $imgName;
  copy($uploadPath, $uploadPathFront);

	$Connection = createS3Connection();
	$file = saveFileAmazonS3($Connection, 'img/posts/'.$imgName, file_get_contents($uploadPath));
  echo $imgName;
} else {
  echo 'No files';
}
?>
