<!DOCTYPE html>
<?php if(!isset($_GET['_escaped_fragment_'])){ ?>
	<html xmlns="http://www.w3.org/1999/xhtml" ng-app="siteApp" id="ng-app" lang="pt-BR">
	<head>
		<meta name="fragment" content="!">
		<title>Ser Viajante: Blog de Viagem</title>
		<meta name="msvalidate.01" content="519EC645329210837C16607F26B03144" />
		<meta name="description" content="Blog de viagens.">
		<meta name="keywords" content="Viagem, Blog">
		<meta name="robots" content="index,follow">
		<meta name="cache-control" content="Public">
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta property="og:title" content="Ser Viajante" />
		<meta property="og:description" content="Blog de viagens." />
		<link rel="stylesheet" type="text/css" href="css/cssreset.css" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/admin.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap-tagsinput.css" />
		<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
		<link rel="stylesheet" href="libs/angular-bootstrap/ui-bootstrap-custom-1.0.3-csp.css" type="text/css" ></link>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular-animate.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular-cookies.min.js" type="application/javascript" ></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular-loader.min.js" type="application/javascript"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular-route.min.js" type="application/javascript"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular-route.min.js" type="application/javascript"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.11/angular-sanitize.min.js" type="application/javascript"></script>

		<link rel="stylesheet" type="text/css" href="libs/angular-text/textAngular.css" />
		<link rel="stylesheet" type="text/css" href="libs/font-awesome/css/font-awesome.min.css" />
		<script src='libs/angular-text/textAngular-rangy.min.js'></script>
		<script src='libs/angular-text/textAngular-sanitize.min.js'></script>
		<script src='libs/angular-text/textAngular.min.js'></script>
		<script src='libs/angular-text/textAngularSetup.js'></script>
		<script src="libs/angular-upload/angular-file-upload.min.js" type="application/javascript"></script>
		<script src="libs/angular-bootstrap/ui-bootstrap-custom-1.0.3.js" type="application/javascript"></script>
		<script src="libs/angular-bootstrap/ui-bootstrap-custom-tpls-1.0.3.min.js" type="application/javascript"></script>

		<script src="js/app.js" type="application/javascript"></script>
		<script src="js/directives.js" type="application/javascript"></script>
		<script src="js/loginController.js" type="application/javascript"></script>
		<script src="js/menuController.js" type="application/javascript"></script>
		<script src="js/postController.js" type="application/javascript"></script>
		<script src="js/routes.js" type="application/javascript"></script>
		<script src="js/services.js" type="application/javascript"></script>
		<script src="https://apis.google.com/js/platform.js" async defer>
		{lang: 'pt-BR'}
		</script>
	</head>

	<body>
		<div class="container">
			<a href="#!/" class="row">
				<img src="img/logo.png">
			</a>

			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<ul class="nav navbar-nav">
						<li><a href="../front">Visualizar Blog</a></li>
						<li><a href="#!/">Posts</a></li>
						<li><a href="#!/menu">Menus</a></li>
					</ul>
				</div>
			</nav>
			<div class="main" ng-view>
			</div>
		</div>
	</body>
	</html>
	<?php
} else{
	include('snapshots.php');
} ?>
