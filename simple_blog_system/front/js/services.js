siteApp.service('siteService', function ($http, $q) {
    var self = this;

    self.getPosts = function(){
      return $http.get('cache/posts.json', {cache: true});
    };

    self.getFrontPosts = function(){
      return $http.get('cache/posts_front.json', {cache: true});
    };

    self.getPostById = function(itemId){
      return $http.get('cache/posts/'+ itemId +'.json', {cache: true});
    };

    self.getMenus = function(){
      return 	$http.get('cache/menu.json', {cache: true}).success(function(data){
        self.menus = data;
      });
    };

    self.getMenuByGroup = function(group){
      var result = $q.defer();

      function findMenu(menus){
        for(var i = 0; i < menus.length; i++){
          var menu = menus[i];
          if(group == menu.group){
            result.resolve(menu);
          } else{
            if(menu.menus && menu.menus.length > 0){
              findMenu(menu.menus);
            }
          }
        }
      }

      self.getMenus().success(function(menus){
        findMenu(menus);
      });
      return result.promise;
    };

    self.getTags = function(){
      return 	$http.get('cache/tags.json', {cache: true});
    };
});
