siteApp.controller("TagDetailCtrl", function($scope, $http, $location, $routeParams, siteService) {
	siteService.getPosts().success(function(data) {
		$scope.selected_tag = $routeParams.tag;
		$scope.posts = data;
	});
});
