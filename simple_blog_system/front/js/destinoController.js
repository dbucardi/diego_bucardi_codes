siteApp.controller("DestinoDetailCtrl", function($scope, $http, $location, $routeParams, siteService) {
	siteService.getPosts().success(function(data) {
		$scope.selected_menu_group = $routeParams.menu_group;
		siteService.getMenuByGroup($routeParams.menu_group).then(function(menu){
			$scope.title = menu.title;
		});
		$scope.posts = data;
	});
});
