siteApp.controller("MenuCtrl", function($scope, $http, siteService) {
  siteService.getMenus().success(function(data) {
      $scope.menus = data;
  });
	$scope.showChilds = function(item){
        item.active = !item.active;
  };

	$scope.hasChildren = function(menu){
		return (menu.menus && menu.menus.length > 0);
	};
});
