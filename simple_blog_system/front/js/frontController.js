siteApp.controller("FrontCtrl", function($scope, $http, $location, $routeParams, siteService) {
	var fragment = getQueryString('_escaped_fragment_');

	if(fragment == undefined ){
		siteService.getFrontPosts().success(function(data) {
		  $scope.posts = data;
		});
		siteService.getTags().success(function(data) {
		  $scope.tags = data;
		});
	} else{
		$location.path(fragment).replace();
	}
});
