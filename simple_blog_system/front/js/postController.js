siteApp.controller("PostDetailCtrl", function($scope, $http, $routeParams, $location, siteService) {
    $scope.itemId =  $routeParams.itemId;
  	siteService.getPostById($scope.itemId).success(function(data) {
  	  $scope.post = data;
    });
});

siteApp.controller("PostListCtrl", function($scope, $http, $routeParams, $location, siteService) {
  	siteService.getPosts().success(function(data) {
      $scope.projects = data;
    });
});
