siteApp.config(['$routeProvider','$locationProvider',
    function($routeProvider, $locationProvider) {
        $routeProvider.
        when('/', {
            templateUrl: 'partials/front.html',
            controller: 'FrontCtrl'
        }).
        when('/post', {
            templateUrl: 'partials/post-list.html',
            controller: 'PostListCtrl'
        }).
        when('/post/:itemId', {
            templateUrl: 'partials/post-detail.html',
            controller: 'PostDetailCtrl'
        }).
        when('/destino/:menu_group', {
            templateUrl: 'partials/destino-detail.html',
            controller: 'DestinoDetailCtrl'
        }).
        when('/tag/:tag', {
            templateUrl: 'partials/tag-detail.html',
            controller: 'TagDetailCtrl'
        }).
        otherwise({
            redirectTo: '/'
        });
		$locationProvider.hashPrefix('!');
    }
]);

siteApp.config(function($httpProvider) {

    $httpProvider.interceptors.push(function($q, $rootScope) {
        return {
            'request': function(config) {
                $rootScope.$broadcast('loading-started');
                return config || $q.when(config);
            },
            'response': function(response) {
                $rootScope.$broadcast('loading-complete');
                return response || $q.when(response);
            }
        };
    });

});

function getQueryString(queryName) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == queryName) {
            return decodeURIComponent(pair[1]);
        }
    }
	return undefined;
}
