<html xmlns="http://www.w3.org/1999/xhtml" ng-app="siteApp" id="ng-app" lang="pt-BR">
	<head>
		<meta name="fragment" content="!">
		<title>Ser Viajante: Blog de Viagem</title>
		<meta name="msvalidate.01" content="519EC645329210837C16607F26B03144" />
		<meta name="description" content="Blog de viagens.">
		<meta name="keywords" content="Viagem, Blog">
		<meta name="robots" content="index,follow">
		<meta name="cache-control" content="Public">
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta property="og:title" content="Ser Viajante" />
		<meta property="og:description" content="Blog de viagens." />

		<!-- Jquery -->
		<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>

		<!-- Bootstrap -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="libs/bootstrap-smartmenus/jquery.smartmenus.bootstrap.css">
		<script src="libs/bootstrap-smartmenus/jquery.smartmenus.min.js" type="application/javascript"></script>

		<!-- Angular -->
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js" type="application/javascript"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js" type="application/javascript"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-cookies.min.js" type="application/javascript"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-loader.min.js" type="application/javascript"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.min.js" type="application/javascript"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.min.js" type="application/javascript"></script>
		<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-sanitize.min.js" type="application/javascript"></script>

		<!-- Angular UI carousel-->
		<script src="libs/ui-bootstrap-carousel/ui-bootstrap-carousel-1.1.1.min.js" type="application/javascript"></script>
		<link rel="stylesheet" href="libs/ui-bootstrap-carousel/ui-bootstrap-carousel-1.1.1-csp.css">
		<script src="libs/ui-bootstrap-carousel/ui-bootstrap-carousel-tpls-1.1.1.min.js" type="application/javascript"></script>

		<!-- Application -->
		<link rel="stylesheet" type="text/css" href="css/cssreset.css" />
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		<script src="js/app.js" type="application/javascript"></script>
		<script src="js/routes.js" type="application/javascript"></script>
		<script src="js/services.js" type="application/javascript"></script>
		<script src="js/directives.js" type="application/javascript"></script>
		<script src="js/frontController.js" type="application/javascript"></script>
		<script src="js/menuController.js" type="application/javascript"></script>
		<script src="js/postController.js" type="application/javascript"></script>
		<script src="js/destinoController.js" type="application/javascript"></script>
		<script src="js/tagController.js" type="application/javascript"></script>
		<script src="js/carouselController.js" type="application/javascript"></script>

		<script src="https://apis.google.com/js/platform.js" async defer>
		{lang: 'pt-BR'}
		</script>
	</head>

	<body>
		<div class="container">
			<div class="margin-bottom">
			  <a href="#!/">
			    <img src="img/logo.png">
			  </a>
			</div>
			<div ng-include="'partials/menu.html'"></div>
			<div class="main" ng-view>
			</div>

			<footer class="row">
				<p>Conteúdo protegido por direitos autorais. Proibida a reprodução total ou parcial sem autorização expressa dos autores.</p>
				<p>Copyright © 2016 Ser Viajante Blog.</p>
			</footer>
		</div>



		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=1191909037523937";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	</body>
</html>
